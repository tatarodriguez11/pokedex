import actionTypes from '../actionTypes/index'

export function addCounter() {
  return { type: actionTypes.INCREMENT }
}

