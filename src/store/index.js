import { createStore } from 'redux'
import actionType from '../actionTypes/index'

const state = {
  counter: 0
}

const reducer = (state, action) => {
  switch (action.type) {
    case actionType.INCREMENT:
      return {
        ...state,
        counter: state.counter + 1
      }

    default:
      return state
  }
}

const store = createStore(
  reducer,
  state
)


export default store
