export const POKEMON = {
  "name": "",
  "weight": {
    "minimum": "",
    "maximum": ""
  },
  "classification": "",
  "height": {
    "minimum": "",
    "maximum": ""
  },
  "resistant": [],
  "attacks": {
    "fast": [],
    "special": []
  },
  "weaknesses": [],
  "evolutions": [],
  "types": [
    "Normal"
  ],
  isLoading: true,
  image: "https://media.giphy.com/media/wbN4PUbwCQDN6/giphy.gif"
}