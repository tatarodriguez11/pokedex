import React from 'react';
import Container from '../../basic/SpecieDescriptionContainer'
import Loader from '../../basic/AttackSlider'


class AttackLoader extends React.Component {
  render() {
    return (
      <Container >
        {
          this.props.attacks.map((attack) => {
            return (
              <Loader type={this.props.type} key={attack.name} damage={attack.damage} attack={attack.name} />

            )
          })
        }
      </Container>
    )
  }
}

export default AttackLoader