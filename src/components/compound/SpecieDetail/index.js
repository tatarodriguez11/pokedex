import React from 'react'
import SpecieContainer from '../../basic/SpecieDescriptionContainer'
import SpecieItem from '../../basic/specieItem'

class SpecieDetail extends React.Component {
  render() {
    return (
      <SpecieContainer>
        <SpecieItem itemContent={this.props.alturaMax} itemName='Height maximum' />
        <SpecieItem itemContent={this.props.alturaMin} itemName='Height minimum' />
        <SpecieItem itemContent={this.props.pesoMax} itemName='Weight maximum' />
        <SpecieItem itemContent={this.props.pesoMin} itemName='Weight minimum' />

      </SpecieContainer>
    )
  }
}

export default SpecieDetail