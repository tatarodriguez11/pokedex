import CardImage from '../../basic/cardImage';
import CardInfoContainer from '../../basic/cardInfoContainer';
import CardTitle from '../../basic/cardTitle';
import CardContainer from '../../basic/cardContainerDetail';
import { Icon } from 'evergreen-ui'
import React from 'react';
import './cardDetail.css';


class cardDetail extends React.Component {
  render() {
    return (
      <CardContainer type={this.props.type}>
        <CardInfoContainer>
          <div className="top-section-detail">
            <CardTitle size="18px" >{this.props.pokemonName}</CardTitle>
            <h2 className="pokemon-number-detail">#{this.props.pokemonNumber}</h2>

          </div>
          <div className="middle-section-detail">
            <p className="classification-detail">{this.props.classification}</p>
            <div className="icons-container">
              <Icon icon="star-empty" size={20} color="#00000077" style={{ margin: "2px" }} />
              <Icon icon="circle" size={18} color="#00000077" />
            </div>
          </div>
          <div className="down-section-detail">

            {
              this.props.types.map(function (type) {
                return (
                  <li className="each-type" key={type}>{type}</li>
                )
              })
            }
          </div>
        </CardInfoContainer>
        <CardImage bottom="10px" src={this.props.pokemonImage} isLoading={this.props.isLoading} />

      </CardContainer>
    )
  }
}

export default cardDetail