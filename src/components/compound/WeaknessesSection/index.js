import React, { Fragment } from 'react'
import SpecieContainer from '../../basic/SpecieDescriptionContainer'
import './weaknesses.css'
import EachType from '../../basic/eachType'

function WeaknessesSection(props) {
  return (
    <SpecieContainer>


      {
        props.weaknesses.map(function (weak) {
          return (
            <EachType key={weak}> {weak} </EachType>

          )
        })
      }

    </SpecieContainer>
  )
}

export default WeaknessesSection