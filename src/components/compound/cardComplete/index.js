import React from 'react';
import CardImage from '../../basic/cardImage';
import CardInfoContainer from '../../basic/cardInfoContainer';
import CardTitle from '../../basic/cardTitle';
import CardContainer from '../../basic/cardContainer';
import { Icon } from 'evergreen-ui'
import './card.css';
import EachType from '../../basic/eachType'



class CardComplete extends React.Component {
  render() {
    return (
      <CardContainer type={this.props.type} onClick={this.props.onClick}>
        <CardInfoContainer>
          <div className="top-section">
            <h2 className="pokemon-number">#{this.props.pokemonNumber}</h2>
            <CardTitle size="18px" >{this.props.pokemonName}</CardTitle>
            <div className="icons-container">
              <Icon icon="star-empty" size={20} color="#00000077" style={{ margin: "5px" }} />
              <Icon icon="circle" size={18} color="#00000077" style={{ margin: "5px" }} />
            </div>

          </div>
          <div className="down-section">

            {
              this.props.types.map(function (type) {
                return (
                  <li className="each-type" key={type}>{type}</li>
                )
              })
            }
          </div>
        </CardInfoContainer>
        <CardImage src={this.props.pokemonImage} />
        {/* <div style={{ display: "flex", flexDirection: "row" }}>

            <div style={{ display: "flex", flexDirection: "column" }}>
              <h2>Classification</h2>
              <p>{this.props.classification}</p>
            </div>

            <div style={{ display: "flex", flexDirection: "column" }}>
              <ul>Types</ul>
              {
                this.props.types.map(function (type) {
                  return (
                    <li>{type}</li>
                  )
                })
              }
            </div>
          </div> */}

      </CardContainer>
    )
  }
}

export default CardComplete