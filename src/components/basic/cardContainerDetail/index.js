import React from 'react';
import './cardContainerDetail.css'

class CardContainerDetail extends React.Component {
  render() {
    return (
      <div className={`card-container-detail ${this.props.type}`}>
        {this.props.children}
      </div>
    )
  }
}

export default CardContainerDetail