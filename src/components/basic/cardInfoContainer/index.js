import React from 'react'
import './cardInfoContainer.css'

function cardImage(props) {
  return (
    <div className="card-info-container">
      {props.children}
    </div>
  )
}

export default cardImage