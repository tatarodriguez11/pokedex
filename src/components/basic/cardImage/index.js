import React from 'react';
import './cardImage.css'

function CardFlag(props) {
  return (

    <div className="image-container" style={{ paddingBottom: props.bottom }}>
      <img src={props.src} className={`image ${props.isLoading ? 'isLoading' : ''}`} />
    </div>

  )
}

export default CardFlag