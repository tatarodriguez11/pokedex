import React from 'react';
import './cardContainer.css'

class CardContainer extends React.Component {
  render() {
    return (
      <div className={`card-container ${this.props.type}`} onClick={this.props.onClick}>
        {this.props.children}
      </div>
    )
  }
}

export default CardContainer