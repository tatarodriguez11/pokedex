import React from 'react'
import './cardClassification.css'


function cardClassification(props) {
  return (
    <div className="classification-container">
      <h2>Classification</h2>
      <p>{props.classification}</p>
    </div>
  )
}

export default cardClassification