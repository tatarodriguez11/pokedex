import React, { Fragment } from 'react';
import './attackSlider.css'


function AttackSlider(props) {

  return (
    <div style={{ display: "flex", flexDirection: "row", margin: "auto", width: "90%", alignItems: "center" }}>
      <p className="attack-name">{props.attack}</p>

      <div className="slider-container">
        <div className={`attack-damage ${props.type}`} style={{ width: `${props.damage}%` }}>
        </div>
      </div>
    </div>
  )
}

export default AttackSlider