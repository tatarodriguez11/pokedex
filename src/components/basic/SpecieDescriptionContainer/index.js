import React from 'react';
import './specieContainer.css'

function SpecieDescriptionContainer(props) {
  return (
    <div className="specie-container">
      {props.children}
    </div>
  )
}

export default SpecieDescriptionContainer