import React, { Fragment } from 'react'
import './specieItem.css'


function SpecieItem(props) {
  return (
    <div style={{ display: "flex", flexDirection: "column", minWidth: "-webkit-fill-available", alignItems:"center" }} >
      <div className="item-container">
        <p className="item-content">{props.itemContent}</p>
      </div>
      <p className="item-name">{props.itemName}</p>
    </div>
  )
}

export default SpecieItem