import React from 'react';
import './cardTitle.css'

function CardTitle(props) {
  return (

    <h1 className="card-title" style={{ color: `${props.color}`, fontSize: `${props.size}` }}>
      {props.children}
    </h1>

  )
}

export default CardTitle