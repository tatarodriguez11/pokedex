import React, { Component, Fragment } from 'react';
import { Provider } from 'react-redux';
import store from '../src/store/index'
import { Route, Switch } from 'react-router-dom';
import Main from '../src/pages/main/index'
import Details from '../src/pages/Details'
import { getPokemons } from '../src/api/index'



class App extends Component {
  state = {
    pokemons: null
  }
  async componentDidMount() {
    const pokemons = await getPokemons()
    this.setState({
      pokemons
    })

  }
  render() {

    return (
      <Fragment>
        <Provider store={store}>
          <Switch>
            <Route exact path="/" component={Main} />
            <Route path="/pokemons/:nombre" component={Details} />

          </Switch>
        </Provider>
      </Fragment>
    );
  }
}

export default App;
