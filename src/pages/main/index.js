import React from 'react'
import { connect } from 'react-redux';
import { addCounter } from '../../actions/index'
import { getPokemons } from '../../api/index'
import CardComplete from '../../components/compound/cardComplete'
import Title from '../../components/basic/cardTitle'
import { Link } from 'react-router-dom'
import './main.css';
import { CSSTransition } from 'react-transition-group';
import logo from '../../pokeball.svg';
import { SearchInput } from 'evergreen-ui'




class Main extends React.Component {

  state = {
    pokemons: null,
    searchValue: ''
  }
  // onClick = () => {
  //   this.props.addCounter()
  // }

  async componentDidMount() {
    const pokemons = await getPokemons()
    this.setState({
      pokemons,
      rawPokemons: pokemons
    })

  }
  onClick = (type) => {


    document.body.classList.add(type);


  }

  onSearchChange = (event) => {

    const nombre = event.target.value
    this.setState({
      searchValue: nombre
    })
    const filteredPokemons = this.state.rawPokemons.filter((pokemon) => {
      return (pokemon.name.toLowerCase().includes(nombre.toLowerCase()))

    })
    this.setState({
      pokemons: filteredPokemons
    })
  }


  render() {


    return (
      <div>
        <Title color="black">The first 151 pokemons are:</Title>


        <CSSTransition
          in={this.state.pokemons === null}
          classNames="pokeball"
          timeout={300}
          unmountOnExit
          onExited={() => this.setState({ showPokemons: true })}
        >
          <div className="pokeball-container">
            <img src={logo} alt="pokeball" className="pokeball-image" />
          </div>
        </CSSTransition>




        <CSSTransition
          in={this.state.showPokemons}
          classNames="pokemon"
          timeout={300}
          unmountOnExit
        >

          <div className="general-container">
            <div className="search-bar-container">
              <SearchInput
                placeholder="Write the name of your favorite pokemon"
                width="100%"
                onChange={this.onSearchChange}
                value={this.state.searchValue}
              />
            </div>

            {
              this.state.pokemons !== null &&
              this.state.pokemons.map((pokemon) => {
                return (

                  <Link to={`/pokemons/${pokemon.name}`} key={pokemon.id} onClick={() => this.onClick(pokemon.types[0])} >
                    <CardComplete

                      type={pokemon.types[0]}

                      pokemonNumber={pokemon.number}
                      pokemonName={pokemon.name}
                      pokemonImage={`https://img.pokemondb.net/sprites/black-white/anim/normal/${pokemon.name.toLowerCase()}.gif`}
                      types={pokemon.types}
                      classification={pokemon.classification}
                    />
                  </Link>
                )
              })
            }
          </div>
        </CSSTransition>



        {/* <button onClick={this.onClick}>counter</button>
        <p>{this.props.counter}</p> */}
      </div>
    )
  }
}

function mapStateToProps(state) {
  return {
    counter: state.counter

  }
}

function mapDispatchToProps(dispatch) {
  return {
    addCounter: () => dispatch(addCounter())
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(Main)