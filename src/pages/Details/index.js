import React, { Fragment } from 'react'
import { getPokemonData } from '../../api/index'
import CardDetail from '../../components/compound/cardDetail'
import './details.css'
import Title from '../../components/basic/cardTitle';
import SpecieDetail from '../../components/compound/SpecieDetail'
import WeaknessesSection from '../../components/compound/WeaknessesSection'
import AttackLoader from '../../components/compound/AttackLoader'
import CardComplete from '../../components/compound/cardComplete'
import { Link } from 'react-router-dom';
import { POKEMON } from '../../constants/plantillaPokemon';
import { CSSTransition } from 'react-transition-group';




const getPokemonImage = (name) => {
  return `https://img.pokemondb.net/sprites/black-white/anim/normal/${name.toLowerCase()}.gif`
}

class Details extends React.Component {

  state = {
    name: null,
    pokemonData: null
  }

  async componentDidMount() {
    const { match: { params } } = this.props

    this.setState({
      name: params.nombre
    })
    const pokemonData = await getPokemonData(`${params.nombre}`)
    this.setState({
      pokemonData

    })
    document.body.classList.add(pokemonData.types[0]);




  }

  componentDidUpdate(prevProps) {
    const { match: { params } } = this.props

    // alert('cambiaste de pokemon')
    if (this.props.location.pathname !== prevProps.location.pathname) {

      // alert(`el nuevo pokemon es ${params.nombre}`)
      this.setState({
        pokemonData: POKEMON,
        showPokemons: false
      })
      document.body.classList.add(POKEMON.types[0]);

      window.scrollTo(0, 0);
      getPokemonData(`${params.nombre}`)
        .then((result) => {
          this.setState({
            pokemonData: result,
            showPokemons: true
          })
          document.body.classList.remove(POKEMON.types[0])

          document.body.classList.add(result.types[0]);

        })

    }

  }
  componentWillUnmount() {
    document.body.classList.remove(this.state.pokemonData.types[0])
  }



  render() {
    return (
      <Fragment>

        {
          this.state.pokemonData !== null &&


          <div className="header-container">


            <CSSTransition
              in={this.state.pokemonData !== null}
              classNames="pokemonCard"
              appear
              mountOnEnter
              timeout={300}
              onEntered={() => this.setState({ showPokemons: true })}
            >

              <CardDetail
                type={this.state.pokemonData.types[0]}
                key={this.state.pokemonData.id}
                pokemonNumber={this.state.pokemonData.number}
                pokemonName={this.state.pokemonData.name}
                pokemonImage={this.state.pokemonData.image || getPokemonImage(this.state.pokemonData.name)}
                types={this.state.pokemonData.types}
                classification={this.state.pokemonData.classification}
                isLoading={this.state.pokemonData.isLoading}
              />
            </CSSTransition>


            <CSSTransition
              in={this.state.showPokemons}
              classNames="pokemonCard"
              timeout={300}
              unmountOnExit
              mountOnEnter

            >
              <div className="stats-container">
                <Title color="#00000077" size="20px">Stats</Title>
                <SpecieDetail
                  alturaMax={this.state.pokemonData.height.maximum}
                  alturaMin={this.state.pokemonData.height.minimum}
                  pesoMax={this.state.pokemonData.weight.maximum}
                  pesoMin={this.state.pokemonData.weight.minimum}


                />
                <Title color="#00000077" size="20px">Weaknesses</Title>
                <WeaknessesSection
                  weaknesses={this.state.pokemonData.weaknesses}
                />
                <Title color="#00000077" size="20px">Resistant</Title>
                <WeaknessesSection
                  weaknesses={this.state.pokemonData.resistant}
                />
                <Title color="#00000077" size="20px">Fast Attacks</Title>
                <AttackLoader
                  type={this.state.pokemonData.types[0]}
                  attacks={this.state.pokemonData.attacks.fast}

                />
                <Title color="#00000077" size="20px">Special Attacks</Title>
                <AttackLoader
                  type={this.state.pokemonData.types[0]}
                  attacks={this.state.pokemonData.attacks.special}

                />
                {
                  console.log(this.state.pokemonData.evolutions)


                }
                {
                  this.state.pokemonData.evolutions !== null &&
                  <Title color="#00000077" size="20px">Evolutions</Title>


                }
                {
                  this.state.pokemonData.evolutions !== null &&

                  this.state.pokemonData.evolutions.map((pokemon) => {
                    return (


                      <Link key={pokemon.id} to={`/pokemons/${pokemon.name}`}>
                        <CardComplete
                          type={pokemon.types[0]}

                          pokemonNumber={pokemon.number}
                          pokemonName={pokemon.name}
                          pokemonImage={`https://img.pokemondb.net/sprites/black-white/anim/normal/${pokemon.name.toLowerCase()}.gif`}
                          types={pokemon.types}
                          classification={pokemon.classification}
                        />
                      </Link>


                    )
                  })


                }
              </div>
            </CSSTransition>




          </div>

        }

      </Fragment>
    )
  }
}

Details.defaultProps = {

}

export default Details