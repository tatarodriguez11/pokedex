import axios from 'axios'


export function getPokemons() {
  return axios({
    url: 'https://graphql-pokemon-dbuhsbemzh.now.sh/',
    method: 'post',
    data: {
      query: `
     query getPokemons { 
  
      pokemons(first:151) {
        
        name
        id
        types
        number
      

      }
      }
      `
    }
  }).then((result) => {

    return result.data.data.pokemons
  }).catch(
    console.log
  )
}

export function getPokemonData(name) {
  return axios({
    url: 'https://graphql-pokemon-dbuhsbemzh.now.sh/',
    method: 'post',
    data: {
      query: `
     query getPokemonData{
        pokemon(name:"${name}"){
          name
          weight {
            minimum
            maximum
          }
          classification
          height{
            minimum
            maximum
          }
          resistant
          attacks{
            fast{
              name
              type
              damage
            }
            special{
              name
              type
              damage
            }
          }
          weaknesses
          fleeRate
          maxCP
          evolutions{
            name
            types
            classification
            number
            id
            
          }
        evolutionRequirements{
          name
          amount
        }
          types
          number
          
        }
      }

      `
    }
  }).then((result) => {

    return result.data.data.pokemon
  }).catch(
    console.log
  )
}

